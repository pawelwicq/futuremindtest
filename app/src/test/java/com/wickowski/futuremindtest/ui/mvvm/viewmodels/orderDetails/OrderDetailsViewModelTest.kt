package com.wickowski.futuremindtest.ui.mvvm.viewmodels.orderDetails

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.nhaarman.mockito_kotlin.*
import com.wickowski.futuremindtest.domain.OrdersService
import com.wickowski.futuremindtest.model.Order
import com.wickowski.futuremindtest.ui.mvvm.viewdata.OrderDetails
import com.wickowski.futuremindtest.ui.mvvm.viewdata.OrderDetailsLoading
import com.wickowski.futuremindtest.ui.mvvm.viewdata.OrderDetailsViewData
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.util.*


class OrderDetailsViewModelTest {

    private val sampleUrl = "http://www.math-info.univ-paris5.fr/~sdurand/Cours/Baboon_color.bmp"
    private val order = Order(1, "test", "Test desc", Date(), sampleUrl, sampleUrl)
    private val orderSingle = Single.just(order)

    val mockOrdersService = mock<OrdersService> {
        on { getOrder(any()) } doReturn orderSingle
    }

    val orderDetailsViewModel = OrderDetailsViewModel(mockOrdersService)

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @Before
    fun makeSchedulersSynchronous() {
        RxJavaPlugins.setInitIoSchedulerHandler { Schedulers.from { command -> command.run() } }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.from { command -> command.run() } }
    }

    @Test
    fun `when load order called then viewmodel asks repository for it`() {
        orderDetailsViewModel.loadOrder(1L)
        verify(mockOrdersService, times(1)).getOrder(any())
    }

    @Test
    fun `live data responds to order download from repository`() {
        val observer = mock<Observer<OrderDetailsViewData>>()
        orderDetailsViewModel.orderDetailsData.observeForever(observer)

        orderDetailsViewModel.loadOrder(1L)

        verify(observer).onChanged(isA<OrderDetailsLoading>())
        verify(observer).onChanged(isA<OrderDetails>())
    }

}