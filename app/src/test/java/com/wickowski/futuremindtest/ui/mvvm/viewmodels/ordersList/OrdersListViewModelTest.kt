package com.wickowski.futuremindtest.ui.mvvm.viewmodels.ordersList

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.nhaarman.mockito_kotlin.isA
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.wickowski.futuremindtest.domain.OrdersService
import com.wickowski.futuremindtest.model.Order
import com.wickowski.futuremindtest.ui.mvvm.viewdata.LocalOrdersList
import com.wickowski.futuremindtest.ui.mvvm.viewdata.OrdersListLoading
import com.wickowski.futuremindtest.ui.mvvm.viewdata.OrdersListViewData
import com.wickowski.futuremindtest.ui.mvvm.viewdata.RemoteOrdersList
import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.util.*

class OrdersListViewModelTest {

    val ordersList = ArrayList<Order>().apply {
        add(Order(1, "tt", "TESTING TESTING TESTING http://test.com.pl/dasda/", Date(), "testUrl", ""))
        add(Order(2, "tt", "TESTING TESTING TESTING https://test.com.pl/dasda/", Date(), "testUrl", ""))
    }

    val mockOrdersService = mock<OrdersService> {
        on { loadRemoteOrders() }.thenReturn(Observable.just(ordersList))
        on { loadLocalOrders() }.thenReturn(Observable.just(ordersList))
    }

    val emptyOrdersService = mock<OrdersService> {
        on { loadRemoteOrders() }.thenReturn(Observable.just(ordersList))
        on { loadLocalOrders() }.thenReturn(Observable.just(ordersList))
    }

    val ordersListViewModel = OrdersListViewModel(mockOrdersService)


    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @Before
    fun makeSchedulersSynchronous() {
        RxJavaPlugins.setInitIoSchedulerHandler { Schedulers.from { command -> command.run() } }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.from { command -> command.run() } }
    }

    @Test
    fun `when load local orders called then viewmodel asks repository for it`() {
        ordersListViewModel.loadOrdersLocally()
        verify(mockOrdersService, times(1)).loadLocalOrders()
        verify(mockOrdersService, times(0)).loadRemoteOrders()
    }

    @Test
    fun `when load remote orders called then viewmodel asks repository for it`() {
        ordersListViewModel.loadRemoteOrders()
        verify(mockOrdersService, times(1)).loadRemoteOrders()
        verify(mockOrdersService, times(0)).loadLocalOrders()
    }

    @Test
    fun `live data responds to successful order download from remote repository`() {
        val observer = mock<Observer<OrdersListViewData>>()
        ordersListViewModel.ordersListData.observeForever(observer)

        ordersListViewModel.loadRemoteOrders()

        verify(observer).onChanged(isA<RemoteOrdersList>())
    }

    @Test
    fun `live data responds to successful order download from local repository`() {
        val observer = mock<Observer<OrdersListViewData>>()
        ordersListViewModel.ordersListData.observeForever(observer)

        ordersListViewModel.loadOrdersLocally()

        verify(observer).onChanged(isA<OrdersListLoading>())
        verify(observer).onChanged(isA<LocalOrdersList>())
    }


}