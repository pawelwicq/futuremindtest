package com.wickowski.futuremindtest.repository.orders.local

import com.nhaarman.mockito_kotlin.*
import com.wickowski.futuremindtest.database.OrdersDao
import com.wickowski.futuremindtest.model.Order
import junit.framework.Assert.*
import org.junit.Test
import java.util.*
import kotlin.collections.ArrayList

class LocalOrdersRepositoryTest {

    private val sampleUrl = "http://www.math-info.univ-paris5.fr/~sdurand/Cours/Baboon_color.bmp"
    private var ordersList = ArrayList<Order>()

    val ordersDaoMock = mock<OrdersDao> {
        on { getAll() } doReturn ordersList
        on { save(any()) }.then { ordersList.addAll(createMockOrdersList()) }
        on { clearAll() }.then { ordersList.clear() }
        on { find(1L) } doReturn (Order(1, "order title 1", "description", Date(), sampleUrl, sampleUrl))
        on { find(2L) }.thenReturn(null)
    }

    private fun createMockOrdersList(): ArrayList<Order> {
        return ArrayList<Order>().apply {
            add(Order(1, "order title 1", "description", Date(), sampleUrl, sampleUrl))
            add(Order(2, "order title 2", "description", Date(), sampleUrl, sampleUrl))
            add(Order(3, "order title 3", "description", Date(), sampleUrl, sampleUrl))
            add(Order(4, "order title 4", "description", Date(), sampleUrl, sampleUrl))
            add(Order(5, "order title 5", "description", Date(), sampleUrl, sampleUrl))
            add(Order(6, "order title 6", "description", Date(), sampleUrl, sampleUrl))
        }
    }

    val localRepository = LocalDbOrdersRepository(ordersDaoMock)

    @Test
    fun `when no data inserted repository is empty`() {
        val orders = localRepository.loadOrders().blockingFirst()

        assertTrue(orders.isEmpty())
    }

    @Test
    fun `when data inserted then repository contains items`() {
        localRepository.saveOrders(createMockOrdersList()).blockingGet()

        val orders = localRepository.loadOrders().blockingFirst()
        assertTrue(!orders.isEmpty())
    }

    @Test
    fun `when data inserted then local repository deletes old items and inserts new ones`() {
        localRepository.saveOrders(createMockOrdersList()).blockingGet()

        verify(ordersDaoMock, times(1)).clearAll()
        verify(ordersDaoMock, times(1)).save(any())
    }

    @Test(expected=Exception::class)
    fun `when no record found then exception thrown`() {
        val order = localRepository.getOrder(2).blockingGet()

        verify(ordersDaoMock).find(2)
        assertNull(order)
    }

    @Test
    fun `record found when repository has order with it's id`() {
        localRepository.saveOrders(createMockOrdersList()).blockingGet()
        val order = localRepository.getOrder(1L).blockingGet()

        assertEquals(1L, order.orderId)
    }


}