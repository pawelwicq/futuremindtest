package com.wickowski.futuremindtest.repository.orders

import com.nhaarman.mockito_kotlin.*
import com.wickowski.futuremindtest.model.Order
import com.wickowski.futuremindtest.repository.orders.local.LocalOrdersRepository
import com.wickowski.futuremindtest.repository.orders.remote.RemoteOrdersDto
import com.wickowski.futuremindtest.repository.orders.remote.RemoteOrdersRepository
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import org.junit.Test
import java.util.*
import kotlin.collections.ArrayList

class OrdersRepositoryTest {

    private val sampleUrl = "http://www.math-info.univ-paris5.fr/~sdurand/Cours/Baboon_color.bmp"
    private var observableLocalOrdersList:  Observable<List<Order>> = createMockLocalOrdersList()
    private var observableRemoteOrdersList: Observable<RemoteOrdersDto> = createMockOrdersDtoObservable()

    val mockLocalRepository = mock<LocalOrdersRepository> {
        on { loadOrders() } doReturn observableLocalOrdersList
        on { saveOrders(any()) } doReturn Completable.complete()
        on { getOrder(any())} doReturn Single.just(Order(1, "order title 1", "description", Date(), sampleUrl, sampleUrl))
    }

    val mockRemoteRepository = mock<RemoteOrdersRepository> {
        on { loadOrders() } doReturn observableRemoteOrdersList
    }

    val ordersRepository = OrdersRepository(mockLocalRepository, mockRemoteRepository)

    private fun createMockOrdersDtoObservable(): Observable<RemoteOrdersDto> {
        val orders = ArrayList<Order>().apply {
            add(Order(1, "order title 1", "description http://some.link/", Date(), sampleUrl, sampleUrl))
            add(Order(2, "order title 2", "description http://some.link/", Date(), sampleUrl, sampleUrl))
            add(Order(3, "order title 3", "description http://some.link/", Date(), sampleUrl, sampleUrl))
            add(Order(4, "order title 4", "description http://some.link/", Date(), sampleUrl, sampleUrl))
            add(Order(5, "order title 5", "description http://some.link/", Date(), sampleUrl, sampleUrl))
            add(Order(6, "order title 6", "description http://some.link/", Date(), sampleUrl, sampleUrl))
        }

        return Observable.just(RemoteOrdersDto(orders))
    }

    private fun createMockLocalOrdersList(): Observable<List<Order>> {
        val orders = ArrayList<Order>().apply {
            add(Order(1, "order title 1", "description http://some.link/", Date(), sampleUrl, sampleUrl))
            add(Order(2, "order title 2", "description http://some.link/", Date(), sampleUrl, sampleUrl))
            add(Order(3, "order title 3", "description http://some.link/", Date(), sampleUrl, sampleUrl))
        }

        return Observable.just(orders)
    }

    @Test
    fun `repository loads only remote orders and saves it locally`() {
        ordersRepository.loadRemoteOrders().blockingLast()

        verify(mockRemoteRepository).loadOrders()
        verify(mockLocalRepository, times(0)).loadOrders()
        verify(mockLocalRepository).saveOrders(any())
    }

    @Test
    fun `repository loads only local orders`() {
        ordersRepository.loadLocalOrders().blockingLast()

        verify(mockRemoteRepository, times(0)).loadOrders()
        verify(mockLocalRepository, times(1)).loadOrders()
    }

    @Test
    fun `local repository searches for order when getOrder called`() {
        ordersRepository.getOrder(1L).blockingGet()

        verify(mockLocalRepository).getOrder(1L)
    }

}