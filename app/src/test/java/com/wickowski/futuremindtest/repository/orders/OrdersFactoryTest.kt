package com.wickowski.futuremindtest.repository.orders

import com.wickowski.futuremindtest.model.Order
import com.wickowski.futuremindtest.repository.orders.remote.RemoteOrdersDto
import org.junit.Assert.assertEquals
import org.junit.Test
import java.util.*

class OrdersFactoryTest {

    private val ordersDto: RemoteOrdersDto = createOrdersDto()

    private fun createOrdersDto(): RemoteOrdersDto {
        val ordersList = ArrayList<Order>().apply {
            add(Order(1, "tt", "TESTING TESTING TESTING http://test.com.pl/dasda/", Date(), "testUrl", ""))
            add(Order(2, "tt", "TESTING TESTING TESTING https://test.com.pl/dasda/", Date(), "testUrl", ""))
        }

        return RemoteOrdersDto(ordersList)
    }

    @Test
    fun `OrdersFactory map ordersDto to list of orders`() {
        val result = OrdersFactory.fromOrdersDto(ordersDto)

        assertEquals("http://test.com.pl/dasda/", result[0].orderUrl)
        assertEquals("https://test.com.pl/dasda/", result[1].orderUrl)
        assertEquals("TESTING TESTING TESTING", result[0].description)
        assertEquals("TESTING TESTING TESTING", result[1].description)
    }


}