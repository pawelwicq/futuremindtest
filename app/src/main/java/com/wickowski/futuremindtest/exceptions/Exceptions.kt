package com.wickowski.futuremindtest.exceptions

class BusinessException(override val message: String) : Exception(message)

class DatabaseException(override val message: String) : Exception(message)