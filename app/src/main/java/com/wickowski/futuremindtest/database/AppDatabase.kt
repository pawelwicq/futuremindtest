package com.wickowski.futuremindtest.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.wickowski.futuremindtest.model.Order


@Database(entities = [Order::class], version = 1)
@TypeConverters(value = [DateTypeConverter::class])
abstract class AppDatabase : RoomDatabase() {

    companion object {
        val DATABASE_NAME = "ordersdb"
    }

    abstract fun ordersDao(): OrdersDao
}