package com.wickowski.futuremindtest.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.wickowski.futuremindtest.model.Order

@Dao
interface OrdersDao {

    @Query("SELECT * FROM `Order`")
    fun getAll(): List<Order>?

    @Query("SELECT * FROM `Order` WHERE orderId = :orderId")
    fun find(orderId: Long): Order?

    @Insert
    fun save(orders: List<Order>)

    @Query("DELETE FROM `Order`")
    fun clearAll()

}