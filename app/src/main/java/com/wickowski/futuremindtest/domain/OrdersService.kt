package com.wickowski.futuremindtest.domain

import com.wickowski.futuremindtest.model.Order
import io.reactivex.Observable
import io.reactivex.Single

interface OrdersService {
    fun loadLocalOrders(): Observable<List<Order>>
    fun loadRemoteOrders(): Observable<List<Order>>
    fun getOrder(orderId: Long): Single<Order>
}