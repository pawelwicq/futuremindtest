package com.wickowski.futuremindtest.repository.orders.local

import com.wickowski.futuremindtest.model.Order
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

interface LocalOrdersRepository {
    fun getOrder(orderId: Long): Single<Order>
    fun saveOrders(orders: List<Order>): Completable
    fun loadOrders(): Observable<List<Order>>
}