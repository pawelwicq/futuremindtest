package com.wickowski.futuremindtest.repository.orders.remote

import com.wickowski.futuremindtest.model.Order

data class RemoteOrdersDto(val data: List<Order>)