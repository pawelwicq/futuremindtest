package com.wickowski.futuremindtest.repository.orders.remote

import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.http.GET

interface RemoteOrdersRepository {
    @GET("test35")
    fun loadOrders(): Observable<RemoteOrdersDto>

    object Factory {
        fun create(retrofit: Retrofit): RemoteOrdersRepository = retrofit.create(RemoteOrdersRepository::class.java)
    }
}