package com.wickowski.futuremindtest.repository.orders

import com.wickowski.futuremindtest.model.Order
import com.wickowski.futuremindtest.repository.orders.remote.RemoteOrdersDto

object OrdersFactory {

    fun fromOrdersDto(ordersDto: RemoteOrdersDto): List<Order> {
        val ordersList = ArrayList<Order>()
        ordersDto.data.forEach { ordersList.add(extractOrderUrlFromDescription(it)) }
        return ordersList
    }

    private fun extractOrderUrlFromDescription(order: Order): Order {
        val index = order.description.lastIndexOf("http")
        val url = order.description.removeRange(0, index).trim()
        val desc = order.description.removeRange(index, order.description.length).trim()
        return Order(order.orderId, order.title, desc, order.modificationDate, order.imageUrl, url)
    }

}