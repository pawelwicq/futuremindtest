package com.wickowski.futuremindtest.repository.orders.local

import com.wickowski.futuremindtest.database.OrdersDao
import com.wickowski.futuremindtest.exceptions.DatabaseException
import com.wickowski.futuremindtest.model.Order
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import timber.log.Timber
import javax.inject.Inject

class LocalDbOrdersRepository @Inject constructor(private val ordersDao: OrdersDao) : LocalOrdersRepository {

    override fun saveOrders(orders: List<Order>): Completable {
        return Completable.create { emitter ->
            ordersDao.clearAll()
            ordersDao.save(orders)
            emitter.onComplete()
        }
    }

    override fun getOrder(orderId: Long): Single<Order> {
        return Single.create<Order> { emitter ->
            try {
                val order = ordersDao.find(orderId)
                        ?: throw DatabaseException("No record found for $orderId")
                emitter.onSuccess(order)
            } catch (ex: Exception) {
                Timber.e(ex)
                emitter.onError(ex)
            }
        }
    }

    override fun loadOrders(): Observable<List<Order>> {
        return Observable.create<List<Order>> { emitter ->
            try {
                val orders = ordersDao.getAll() ?: ArrayList()
                emitter.onNext(orders)
                emitter.onComplete()
            } catch (ex: Exception) {
                emitter.onError(ex)
            }
        }
    }

}