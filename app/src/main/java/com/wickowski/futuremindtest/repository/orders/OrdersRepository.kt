package com.wickowski.futuremindtest.repository.orders

import com.wickowski.futuremindtest.domain.OrdersService
import com.wickowski.futuremindtest.model.Order
import com.wickowski.futuremindtest.repository.orders.local.LocalOrdersRepository
import com.wickowski.futuremindtest.repository.orders.remote.RemoteOrdersRepository
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class OrdersRepository @Inject constructor(private val localRepository: LocalOrdersRepository,
                                           private val remoteRepository: RemoteOrdersRepository) : OrdersService {

    override fun loadLocalOrders(): Observable<List<Order>> {
        return loadFromLocalDb()
    }

    override fun loadRemoteOrders(): Observable<List<Order>> {
        return loadFromRemoteApi()
    }

    override fun getOrder(orderId: Long): Single<Order> {
        return localRepository.getOrder(orderId)
    }

    private fun loadFromLocalDb(): Observable<List<Order>> {
        return localRepository.loadOrders()
                .doOnNext { Timber.d("got ${it.size} rows from db") }
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
    }

    private fun loadFromRemoteApi(): Observable<List<Order>> {
        return remoteRepository.loadOrders()
                .map { dto -> OrdersFactory.fromOrdersDto(dto) }
                .doOnNext { orders ->
                    Timber.d("got ${orders.size} from api")
                    storeOrdersInLocalDb(orders)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
    }

    private fun storeOrdersInLocalDb(orders: List<Order>?) {
        orders?.let {
            localRepository.saveOrders(it)
                    .observeOn(Schedulers.io())
                    .subscribeOn(Schedulers.io())
                    .subscribe { Timber.d("orders refreshed in local db") }
        }
    }

}