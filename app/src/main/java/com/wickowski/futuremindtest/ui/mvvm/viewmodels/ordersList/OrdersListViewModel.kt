package com.wickowski.futuremindtest.ui.mvvm.viewmodels.ordersList

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.wickowski.futuremindtest.domain.OrdersService
import com.wickowski.futuremindtest.ui.mvvm.viewdata.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.io.IOException
import javax.inject.Inject

class OrdersListViewModel @Inject constructor(private val ordersService: OrdersService) : ViewModel() {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    val ordersListData = MutableLiveData<OrdersListViewData>()

    fun loadOrdersLocally() {
        loadOrdersFromLocalStorage()
    }

    private fun loadOrdersFromLocalStorage() {
        compositeDisposable.add(ordersService.loadLocalOrders()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .doOnSubscribe { ordersListData.postValue(OrdersListLoading()) }
                .subscribe({ list ->
                    ordersListData.postValue(LocalOrdersList(list))
                }, { throwable ->
                    Timber.e(throwable)
                    if (throwable is IOException) {
                        ordersListData.postValue(OrdersListError(throwable))
                    }
                }))
    }

    fun loadRemoteOrders() {
        compositeDisposable.add(ordersService.loadRemoteOrders()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe({ list ->
                    if (list.isEmpty()) ordersListData.postValue(OrdersListEmpty())
                    else ordersListData.postValue(RemoteOrdersList(list))
                }, { throwable ->
                    ordersListData.postValue(OrdersListError(throwable))
                }))
    }

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }

}