package com.wickowski.futuremindtest.ui.activities

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.wickowski.futuremindtest.R
import kotlinx.android.synthetic.main.activity_single_fragment.*

abstract class SingleFragmentActivity : AppCompatActivity() {

    private var containerFragment: Fragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_fragment)
        containerFragment = supportFragmentManager.findFragmentById(fragmentContainer.id) ?: createFragment()
        loadContainerFragment()
    }

    private fun loadContainerFragment() {
        supportFragmentManager.beginTransaction()
                .replace(fragmentContainer.id, containerFragment)
                .commit()
    }

    abstract fun createFragment(): Fragment

}