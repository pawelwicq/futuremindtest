package com.wickowski.futuremindtest.ui.mvvm.viewmodels.orderDetails

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.wickowski.futuremindtest.domain.OrdersService
import com.wickowski.futuremindtest.exceptions.BusinessException
import com.wickowski.futuremindtest.ui.mvvm.viewdata.OrderDetails
import com.wickowski.futuremindtest.ui.mvvm.viewdata.OrderDetailsError
import com.wickowski.futuremindtest.ui.mvvm.viewdata.OrderDetailsLoading
import com.wickowski.futuremindtest.ui.mvvm.viewdata.OrderDetailsViewData
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class OrderDetailsViewModel @Inject constructor(private val ordersService: OrdersService) : ViewModel() {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    val orderDetailsData = MutableLiveData<OrderDetailsViewData>()

    fun loadOrder(orderId: Long) {
        compositeDisposable.add(
                ordersService.getOrder(orderId)
                        .doOnSubscribe { orderDetailsData.postValue(OrderDetailsLoading()) }
                        .observeOn(Schedulers.io())
                        .subscribeOn(Schedulers.io())
                        .subscribe({ order ->
                            orderDetailsData.postValue(OrderDetails(order))
                        }, { throwable ->
                            Timber.e(throwable)
                            if (throwable is BusinessException)
                                orderDetailsData.postValue(OrderDetailsError(throwable))
                        }))
    }

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }
}