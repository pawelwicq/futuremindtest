package com.wickowski.futuremindtest.ui.activities.ordersList

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.wickowski.futuremindtest.OrdersApp
import com.wickowski.futuremindtest.R
import com.wickowski.futuremindtest.di.components.OrderAppComponent
import com.wickowski.futuremindtest.exceptions.BusinessException
import com.wickowski.futuremindtest.model.Order
import com.wickowski.futuremindtest.ui.activities.orderDetails.OrderDetailActivity
import com.wickowski.futuremindtest.ui.adapters.OrdersListAdapter
import com.wickowski.futuremindtest.ui.fragments.orderDetails.OrderDetailsFragment
import com.wickowski.futuremindtest.ui.mvvm.viewdata.*
import com.wickowski.futuremindtest.ui.mvvm.viewmodels.ordersList.OrdersListViewModel
import com.wickowski.futuremindtest.util.NetworkUtils
import kotlinx.android.synthetic.main.activity_orders_list.*
import kotlinx.android.synthetic.main.progress_layout.view.*
import javax.inject.Inject

class OrdersListActivity : AppCompatActivity() {

    private var masterDetailMode = false

    private var ordersListAdapter = OrdersListAdapter(::orderSelected)

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val appComponent: OrderAppComponent by lazy {
        (application as OrdersApp).appComponent
    }

    private lateinit var viewModel: OrdersListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)
        setContentView(R.layout.activity_orders_list)
        initView()
        viewModel = ViewModelProviders.of(this, viewModelFactory)[OrdersListViewModel::class.java]
        initViewModelObservers()
    }

    private fun initView() {
        detailContainer?.let { masterDetailMode = true }
        val layoutManager = LinearLayoutManager(this)
        ordersList.addItemDecoration(DividerItemDecoration(ordersList.context, layoutManager.orientation))
        ordersList.layoutManager = layoutManager
        ordersList.setHasFixedSize(true)
        ordersList.adapter = ordersListAdapter
        swipeRefreshLayout.setOnRefreshListener { viewModel.loadRemoteOrders() }
    }

    private fun initViewModelObservers() {
        viewModel.ordersListData.observe(this, Observer(::handleOrdersData))
        viewModel.loadOrdersLocally()
    }

    private fun handleOrdersData(ordersData: OrdersListViewData?) {
        when (ordersData) {
            is OrdersListLoading -> showProgressView()
            is OrdersListError -> showError(ordersData.throwable)
            is OrdersListEmpty -> showEmptyView()
            is LocalOrdersList -> handleLocalOrders(ordersData)
            is RemoteOrdersList -> {
                toastMessage(resources.getString(R.string.data_reloaded_from_api))
                showOrders(ordersData.list)
            }
            else -> throw IllegalArgumentException("Argument $ordersData is not supported")
        }
    }

    private fun handleLocalOrders(ordersData: LocalOrdersList) {
        showOrders(ordersData.list)
        if (ordersData.list.isEmpty() && NetworkUtils.isOnline(this)) {
            showProgressView()
            viewModel.loadRemoteOrders()
        }
    }

    private fun orderSelected(order: Order) {
        if (masterDetailMode) loadDetailFragment(order)
        else loadDetailActivity(order)
    }

    private fun showProgressView() {
        ordersListProgress.progressMessage.text = resources.getString(R.string.loading_orders)
        ordersList.visibility = View.INVISIBLE
        ordersListProgress.visibility = View.VISIBLE
        emptyListLayout.visibility = View.INVISIBLE
    }

    private fun showEmptyView() {
        swipeRefreshLayout.isRefreshing = false
        ordersList.visibility = View.INVISIBLE
        ordersListProgress.visibility = View.INVISIBLE
        emptyListLayout.visibility = View.VISIBLE
    }

    private fun showOrders(orders: List<Order>) {
        swipeRefreshLayout.isRefreshing = false
        ordersListAdapter.setOrders(orders)
        if (orders.isEmpty()) {
            showEmptyView()
        } else {
            ordersList.visibility = View.VISIBLE
            ordersListProgress.visibility = View.INVISIBLE
            emptyListLayout.visibility = View.INVISIBLE
        }
    }

    private fun showError(throwable: Throwable) {
        swipeRefreshLayout.isRefreshing = false
        val message = when (throwable) {
            is BusinessException -> throwable.message
            else -> resources.getString(R.string.error_message)
        }
        toastMessage(message)
    }

    private fun toastMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    private fun loadDetailActivity(order: Order) {
        OrderDetailActivity.getStartActivityIntent(this, order.orderId).let {
            startActivity(it)
        }
    }

    private fun loadDetailFragment(order: Order) {
        detailContainer?.let {
            supportFragmentManager.beginTransaction()
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                    .replace(it.id, OrderDetailsFragment.getInstance(order.orderId))
                    .commit()
        }
    }

}