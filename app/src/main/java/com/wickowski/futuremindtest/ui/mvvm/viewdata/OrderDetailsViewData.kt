package com.wickowski.futuremindtest.ui.mvvm.viewdata

import com.wickowski.futuremindtest.exceptions.BusinessException
import com.wickowski.futuremindtest.model.Order

sealed class OrderDetailsViewData

class OrderDetailsLoading : OrderDetailsViewData()
class OrderNotFound : OrderDetailsViewData()
class OrderDetailsError(val exception: BusinessException) : OrderDetailsViewData()
class OrderDetails(val order: Order): OrderDetailsViewData()