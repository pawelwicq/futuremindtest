package com.wickowski.futuremindtest.ui.fragments.orderDetails

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.graphics.Bitmap
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import com.wickowski.futuremindtest.OrdersApp
import com.wickowski.futuremindtest.R
import com.wickowski.futuremindtest.di.components.OrderAppComponent
import com.wickowski.futuremindtest.exceptions.BusinessException
import com.wickowski.futuremindtest.exceptions.DatabaseException
import com.wickowski.futuremindtest.model.Order
import com.wickowski.futuremindtest.ui.mvvm.viewdata.*
import com.wickowski.futuremindtest.ui.mvvm.viewmodels.orderDetails.OrderDetailsViewModel
import com.wickowski.futuremindtest.util.NetworkUtils
import kotlinx.android.synthetic.main.fragment_order_details.view.*
import kotlinx.android.synthetic.main.progress_layout.view.*
import javax.inject.Inject


class OrderDetailsFragment : Fragment() {

    companion object {
        private val ORDER_ID = "OrderDetailsFragment.ORDER_ID"

        fun getInstance(orderId: Long): OrderDetailsFragment {
            val fragment = OrderDetailsFragment()
            val args = Bundle().apply { putLong(ORDER_ID, orderId) }
            fragment.arguments = args
            return fragment
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val appComponent: OrderAppComponent by lazy {
        (context?.applicationContext as OrdersApp).appComponent
    }

    private lateinit var webView: WebView
    private lateinit var progressView: View
    private lateinit var noDataView: View

    private var orderId: Long = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_order_details, container, false)
        initView(rootView)

        orderId = arguments?.getLong(ORDER_ID) ?: -1

        context?.let {
            if (!NetworkUtils.isOnline(it)) {
                showNoDataView()
                val message = resources.getString(R.string.no_connection_error)
                showError(BusinessException(message))
            } else {
                val vm = ViewModelProviders.of(this, viewModelFactory)[OrderDetailsViewModel::class.java]
                observeViewModel(vm)
            }
        }

        return rootView
    }

    private fun initView(rootView: View) {
        webView = rootView.detailWebView
        progressView = rootView.orderDetailsProgress
        noDataView = rootView.noDataLayout
        webView.settings.javaScriptEnabled = true
        webView.webViewClient = OrderWebViewClient()
    }

    private fun observeViewModel(vm: OrderDetailsViewModel) {
        vm.orderDetailsData.observe(this, Observer(::handleOrderDetailsData))
        vm.loadOrder(orderId)
    }

    private fun handleOrderDetailsData(orderData: OrderDetailsViewData?) = when (orderData) {
        is OrderDetailsLoading -> showProgressView()
        is OrderDetailsError -> showError(orderData.exception)
        is OrderNotFound -> showOrderNotFound()
        is OrderDetails -> showOrderDetails(orderData.order)
        else -> throw IllegalArgumentException("Argument $orderData is not supported")
    }

    private fun showOrderDetails(order: Order) {
        webView.loadUrl(order.orderUrl)
    }

    private fun showWebView() {
        progressView.visibility = View.INVISIBLE
        noDataView.visibility = View.INVISIBLE
        webView.visibility = View.VISIBLE
    }

    private fun showOrderNotFound() {
        progressView.visibility = View.INVISIBLE
        progressView.visibility = View.VISIBLE
        noDataView.visibility = View.INVISIBLE
    }

    private fun showError(throwable: Throwable) {
        val message = when (throwable) {
            is BusinessException -> throwable.message
            is DatabaseException -> resources.getString(R.string.error_message)
            else -> resources.getString(R.string.not_known_error)
        }
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    private fun showProgressView() {
        webView.visibility = View.INVISIBLE
        progressView.progressMessage.text = resources.getString(R.string.loading_order_details)
        progressView.visibility = View.VISIBLE
        noDataView.visibility = View.INVISIBLE
    }

    private fun showNoDataView() {
        webView.visibility = View.INVISIBLE
        progressView.visibility = View.INVISIBLE
        noDataView.visibility = View.VISIBLE
    }

    private inner class OrderWebViewClient : WebViewClient() {
        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            showProgressView()
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            showWebView()
        }
    }

}