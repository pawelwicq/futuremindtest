package com.wickowski.futuremindtest.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.wickowski.futuremindtest.R
import com.wickowski.futuremindtest.model.Order
import com.wickowski.futuremindtest.ui.views.OrderView

class OrdersListAdapter(private val onItemClick: (Order) -> Unit) : RecyclerView.Adapter<OrdersListAdapter.OrderViewHolder>() {

    private val ordersList: MutableList<Order> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.order_list_item, parent, false) as OrderView
        return OrderViewHolder(view)
    }

    override fun getItemCount() = ordersList.size

    override fun onBindViewHolder(holder: OrderViewHolder, position: Int) {
        holder.bindOrder(ordersList[position])
    }

    fun setOrders(orders: List<Order>) {
        ordersList.clear()
        ordersList.addAll(orders)
        ordersList.sortBy { it.orderId }
        notifyDataSetChanged()
    }

    inner class OrderViewHolder(var orderView: OrderView) : RecyclerView.ViewHolder(orderView) {

        private lateinit var order: Order

        init {
            orderView.setOnClickListener { onItemClick(order) }
        }

        fun bindOrder(order: Order) {
            this.order = order
            orderView.setOrder(order)
        }

    }
}
