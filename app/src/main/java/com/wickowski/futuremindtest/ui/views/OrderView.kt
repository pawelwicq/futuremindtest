package com.wickowski.futuremindtest.ui.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.wickowski.futuremindtest.BuildConfig
import com.wickowski.futuremindtest.R
import com.wickowski.futuremindtest.model.Order
import kotlinx.android.synthetic.main.order_view.view.*
import java.text.SimpleDateFormat

class OrderView(context: Context, attributeSet: AttributeSet) : LinearLayout(context, attributeSet) {

    private val dateFormatter = SimpleDateFormat(BuildConfig.DATE_FORMAT)

    private val orderImage: ImageView
    private val orderTitle: TextView
    private val orderDescription: TextView
    private val orderModificationDate: TextView

    init {
        val root = LayoutInflater.from(context).inflate(R.layout.order_view, this, true)
        orderImage = root.orderImage
        orderTitle = root.orderTitle
        orderDescription = root.orderDescription
        orderModificationDate = root.orderModificationDate

    }

    fun setOrder(order: Order) {
        orderTitle.text = if(order.title.isEmpty()) resources.getString(R.string.no_title) else order.title
        orderDescription.text = order.description
        orderModificationDate.text = dateFormatter.format(order.modificationDate)
        Picasso.get()
                .load(order.imageUrl)
                .placeholder(R.drawable.order_image_placeholder)
                .error(R.drawable.order_image_placeholder)
                .into(orderImage)
    }

}