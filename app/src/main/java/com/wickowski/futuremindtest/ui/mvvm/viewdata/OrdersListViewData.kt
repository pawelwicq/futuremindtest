package com.wickowski.futuremindtest.ui.mvvm.viewdata

import com.wickowski.futuremindtest.model.Order

sealed class OrdersListViewData

class OrdersListLoading : OrdersListViewData()
class OrdersListEmpty : OrdersListViewData()
class OrdersListError(val throwable: Throwable): OrdersListViewData()
class RemoteOrdersList(val list: List<Order>) : OrdersListViewData()
class LocalOrdersList(val list: List<Order>) : OrdersListViewData()