package com.wickowski.futuremindtest.ui.activities.orderDetails

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.wickowski.futuremindtest.ui.fragments.orderDetails.OrderDetailsFragment
import com.wickowski.futuremindtest.ui.activities.SingleFragmentActivity

class OrderDetailActivity : SingleFragmentActivity() {

    companion object {
        private val ORDER_ID_EXTRA = "OrderDetailActivity.ORDER_ID_EXTRA"

        fun getStartActivityIntent(ctx: Context, orderId: Long) =
                Intent(ctx, OrderDetailActivity::class.java).apply {
                    putExtra(ORDER_ID_EXTRA, orderId)
                }
    }

    private var orderId: Long = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        orderId = intent.getLongExtra(ORDER_ID_EXTRA, -1)
        if (orderId < 0) throw IllegalStateException("Intent didn't contain order id")
        super.onCreate(savedInstanceState)
    }

    override fun createFragment() = OrderDetailsFragment.getInstance(orderId)

}
