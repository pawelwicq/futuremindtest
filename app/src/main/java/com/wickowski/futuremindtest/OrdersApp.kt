package com.wickowski.futuremindtest

import android.app.Application
import com.wickowski.futuremindtest.di.components.DaggerOrderAppComponent
import com.wickowski.futuremindtest.di.components.OrderAppComponent
import com.wickowski.futuremindtest.di.modules.OrderAppModule
import timber.log.Timber

class OrdersApp : Application() {

    val appComponent: OrderAppComponent by lazy {
        DaggerOrderAppComponent
                .builder()
                .orderAppModule(OrderAppModule(this))
                .build()
    }

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
    }

}