package com.wickowski.futuremindtest.di.modules

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import com.wickowski.futuremindtest.database.AppDatabase
import com.wickowski.futuremindtest.database.OrdersDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class OrderAppModule(application: Application) {

    private val context: Context = application.applicationContext

    @Provides
    @Singleton
    fun provideContext(): Context = context

    @Provides
    @Singleton
    fun provideDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, AppDatabase.DATABASE_NAME).build()
    }

    @Provides
    @Singleton
    fun provideOrdersDao(appDatabase: AppDatabase): OrdersDao = appDatabase.ordersDao()

}