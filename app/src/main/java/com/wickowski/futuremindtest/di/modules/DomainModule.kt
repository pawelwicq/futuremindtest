package com.wickowski.futuremindtest.di.modules

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.wickowski.futuremindtest.BuildConfig
import com.wickowski.futuremindtest.database.OrdersDao
import com.wickowski.futuremindtest.domain.OrdersService
import com.wickowski.futuremindtest.repository.orders.OrdersRepository
import com.wickowski.futuremindtest.repository.orders.local.LocalDbOrdersRepository
import com.wickowski.futuremindtest.repository.orders.local.LocalOrdersRepository
import com.wickowski.futuremindtest.repository.orders.remote.RemoteOrdersRepository
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class DomainModule {

    @Provides
    @Singleton
    fun providesGson(): Gson {
        return GsonBuilder()
                .setDateFormat(BuildConfig.DATE_FORMAT)
                .create()
    }

    @Provides
    @Singleton
    fun providesRetrofit(gson: Gson): Retrofit {
        return Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(BuildConfig.BASE_URL)
                .build()
    }

    @Provides
    @Singleton
    fun providesOrdersService(localOrdersRepository: LocalOrdersRepository,
                              remoteOrdersRepository: RemoteOrdersRepository): OrdersService {
        return OrdersRepository(localOrdersRepository, remoteOrdersRepository)
    }

    @Provides
    @Singleton
    fun providesRemoteOrdersRepository(retrofit: Retrofit): RemoteOrdersRepository {
        return RemoteOrdersRepository.Factory.create(retrofit)
    }

    @Provides
    @Singleton
    fun providesLocalOrdersRepository(ordersDao: OrdersDao): LocalOrdersRepository = LocalDbOrdersRepository(ordersDao)

}