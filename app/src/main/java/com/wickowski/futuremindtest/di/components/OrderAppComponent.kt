package com.wickowski.futuremindtest.di.components

import com.wickowski.futuremindtest.di.modules.DomainModule
import com.wickowski.futuremindtest.di.modules.OrderAppModule
import com.wickowski.futuremindtest.di.modules.ViewModelModule
import com.wickowski.futuremindtest.ui.activities.ordersList.OrdersListActivity
import com.wickowski.futuremindtest.ui.fragments.orderDetails.OrderDetailsFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [OrderAppModule::class, DomainModule::class, ViewModelModule::class])
interface OrderAppComponent {
    fun inject(ordersListActivity: OrdersListActivity)
    fun inject(orderDetailsFragment: OrderDetailsFragment)
}