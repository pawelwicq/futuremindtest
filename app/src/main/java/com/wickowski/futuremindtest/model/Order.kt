package com.wickowski.futuremindtest.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity
data class Order(@PrimaryKey val orderId: Long,
                 val title: String,
                 val description: String,
                 val modificationDate: Date,
                 @SerializedName("image_url") val imageUrl: String,
                 val orderUrl: String)
